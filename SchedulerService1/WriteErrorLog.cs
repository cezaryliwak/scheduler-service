﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SchedulerService1
{
    class WriteErrorLog
    {
        public WriteErrorLog()
        {

        }
        public static void ErrorLog(string message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":  " + message);
                sw.Flush();
                sw.Close();

            }
            catch
            {

            }
        }
        public static void ErrorLog(Exception message)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\LogFile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + ":  " + message);
                sw.Flush();
                sw.Close();

            }
            catch
            {

            }
        }
    }
}
