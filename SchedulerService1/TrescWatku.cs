﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace SchedulerService1
{
    public class TrescWatku
    {
        #region funkcja wykonująca pojedyncze zapytanie do bazy danych
        public void zapytanie(object wybranySerwer)           //Funkcja ma za zadanie wykonanie jednego zapytania do bazy danych
        {
            int dlugoscSleep = 0;
            do
            {
                try
                {
                Console.WriteLine("nazwa serwera pojedyncze " + wybranySerwer.ToString());
                StreamReader sr = new StreamReader("C:\\ProgramData\\Schedulerv1\\" + wybranySerwer.ToString() + ".xml");
                XmlSerializer serializer = new XmlSerializer(typeof(UstawieniaSerweraPlik));
                UstawieniaSerweraPlik usp = (UstawieniaSerweraPlik)serializer.Deserialize(sr);

                string Username = usp.Username.ToString();
                string Password = usp.Password.ToString();
                string Userwer = usp.SQLserverName1.ToString();
                string UnazwaBazyDanych = usp.NazwaBazyDanych.ToString();
                string Uzapytanie = usp.ZapytanieSQL.ToString();
                
                
                DateTime obecnaData = new DateTime();
                obecnaData = DateTime.Now;



                if (usp.Wlaczony == true)        //sprawdza czy job jest włączony
                {
                        #region zapytanie pojedynczez
                        if (usp.Schedule == true)    //zapytaie pojedyncze
                    {
                       DateTime dataZPliku = usp.OneTimeDate;
                       string stringDataObecna = String.Format("{0:d/M/yyyy HH:mm:ss}", obecnaData);   //ustawienie takiego samego formatu daty
                       string stringdataZPliku = String.Format("{0:d/M/yyyy HH:mm:ss}", dataZPliku);   //ustawieie takiego samego formatu daty
                       if(stringdataZPliku.Equals(stringDataObecna))
                        {
                            if(usp.Authentication == true)
                            {
                                ConnectionDB cdb = new ConnectionDB(Userwer, UnazwaBazyDanych);
                                cdb.zapytanie(Uzapytanie.ToString(), wybranySerwer.ToString());
                                sr.Close();
                                break;
                            }
                            else if(usp.Authentication == false)
                            {
                                ConnectionDB cdb = new ConnectionDB(Username, Password, Userwer, UnazwaBazyDanych);
                                cdb.zapytanie(Uzapytanie.ToString(), wybranySerwer.ToString());
                                sr.Close();
                                break;
                            }
                        }
                        
                    }
                        #endregion

                        else if (usp.Schedule == false)    //zapytanie okresowe
                    {
                        DateTime dataZPliku = usp.RecuringStartDate1;
                       // string stringDataObecna = string.Format("{0:d/M/yyyy HH:mm:ss}", obecnaData);   //ustawienie takiego samego formatu daty
                           
                        string stringdataZPliku = string.Format("{0:d/M/yyyy HH:mm:ss}", dataZPliku);   //ustawieie takiego samego formatu daty
                            DateTime DatetimedataObeca = DateTime.Parse(obecnaData.ToString());              //zamiana string na datetime ( obie daty będą w tym samym formacie daty)
                            DateTime DatetimeDataZPliku = DateTime.Parse(dataZPliku.ToString());             //zamiana string na datetime ( obie daty będą w tym samym formacie daty)

                            if (DatetimeDataZPliku.CompareTo(DatetimedataObeca)==-1)
                            {
                                if (usp.Authentication == true)
                                {
                                    ConnectionDB cdb = new ConnectionDB(Userwer, UnazwaBazyDanych);
                                    cdb.zapytanie(Uzapytanie.ToString(), wybranySerwer.ToString());

                                    switch (usp.Time)
                                    {
                                        case "Minute":
                                            {
                                                sr.Close();
                                                dlugoscSleep = 59000;
                                                aktualizujPlik("Minute", obecnaData.AddMinutes(int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                            }
                                            continue;

                                        case "Hour":
                                            {
                                                sr.Close();
                                                dlugoscSleep = int.Parse(usp.Occurs) * 3600000;
                                                aktualizujPlik("Hour", obecnaData.AddHours(int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                            }
                                            continue;
                                        case "Day":
                                            {
                                                sr.Close();
                                                dlugoscSleep = int.Parse(usp.Occurs) * 86400000;
                                                aktualizujPlik("Day", obecnaData.AddDays(int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                            }
                                            continue;
                                            //---------------------------------------Prawdopodobnie wykracza poza zakres wartości int-------------------------------------------
                                        //case "Week":
                                        //    {
                                        //        sr.Close();
                                        //        aktualizujPlik("Week", dataZPliku.AddDays(7 * int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                        //    }
                                        //    continue;
                                        //case "Month":
                                        //    {
                                        //        sr.Close();
                                        //        aktualizujPlik("Month", dataZPliku.AddMonths(int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                        //    }
                                        //    continue;
                                            //-----------------------------------------------------------------------------------------------------------------------------------
                                    }
                                    System.Threading.Thread.Sleep(dlugoscSleep);

                                }
                                else if(usp.Authentication == false)
                                {
                                    ConnectionDB cdb = new ConnectionDB(Username, Password, Userwer, UnazwaBazyDanych);
                                    cdb.zapytanie(Uzapytanie.ToString(), wybranySerwer.ToString());
                                    switch (usp.Time)
                                    {

                                        case "Minute":
                                            {
                                                sr.Close();
                                                dlugoscSleep = int.Parse(usp.Occurs) *  59000;
                                                aktualizujPlik("Minute", obecnaData.AddMinutes(int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                            }
                                            continue;

                                        case "Hour":
                                            {
                                                sr.Close();
                                                dlugoscSleep = int.Parse(usp.Occurs) * 3600000;
                                                aktualizujPlik("Hour", obecnaData.AddHours(int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                            }
                                            continue;
                                        case "Day":
                                            {
                                                sr.Close();
                                                dlugoscSleep = int.Parse(usp.Occurs) * 86400000;
                                                aktualizujPlik("Day", obecnaData.AddDays(int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                            }
                                            continue;
                                        //case "Week":
                                        //    {
                                        //        sr.Close();
                                        //        dlugoscSleep = int.Parse(usp.Occurs) * 86400000 * 7;
                                        //        aktualizujPlik("Week", dataZPliku.AddDays(7 * int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                        //    }
                                        //    continue;
                                        //case "Month":
                                        //    {
                                        //        sr.Close();
                                        //        aktualizujPlik("Month", dataZPliku.AddMonths(int.Parse(usp.Occurs)), wybranySerwer.ToString());
                                        //    }
                                        //    continue;
                                    }
                                    System.Threading.Thread.Sleep(dlugoscSleep);

                                }
                            }
                    }
                }
                else if(usp.Wlaczony == false)
                    {
                        sr.Close();
                        break;
                    }
                    sr.Close();
                    System.Threading.Thread.Sleep(1000);
                }
                catch (Exception e)
                {
                    WriteErrorLog.ErrorLog(e);
                    break;
                }

            } while (1 == 1);
           

        }
        #endregion



        #region aktualizacja pliku xml BARDZO DO PRZEMYSLENIA NIE POWINNO TAK BYC, ALE NIE BYŁO CZASU INACZEJ

        public void aktualizujPlik(object wybranySerwer)        // ta funkcja ma za zadanie aktualizować plik xml       konkretnie chodzi o przycisk włącz - który ma być ustawiany na false
        {
            try
            {
                StreamReader sr = new StreamReader("C:\\ProgramData\\Schedulerv1\\" + wybranySerwer.ToString() + ".xml");

                XmlSerializer serializer = new XmlSerializer(typeof(UstawieniaSerweraPlik));
                UstawieniaSerweraPlik usp = (UstawieniaSerweraPlik)serializer.Deserialize(sr);
                DateTime dt = new DateTime();

                string username = usp.Username.ToString();                                     //przechowuje login do bazy danych
                string password = usp.Password.ToString();                                     //przechowyje haslo potrzebne do logowania do bazy danych
                string serverName = wybranySerwer.ToString();
                string SQLserverName = usp.SQLserverName1.ToString();                                 //przechowuje nazwe instancji
                string nazwaBazyDanych = usp.NazwaBazyDanych.ToString();                       //przechowuje nazwe bazy danych
                string zapytanieSQl = usp.ZapytanieSQL.ToString();                                //przechowuje zapytanie sql
                bool wlaczony = usp.Wlaczony;                                      // przechowuje true lub false czy job ma działac czy nie
                bool schedule = usp.Schedule;

                DateTime OneTimeDate;

                DateTime RecurigStartDate;                  // przechowyje date rozpoczecia cyklicznego wykonywania job'a

                string occurs = "";                                          //co ile minut/godzin/dni/tygodni/miesiecy ma być wykonywane zapytanie
                string time = "";

                if (usp.Schedule = true)                                                            // przechowuje true lub false czy job ma byc wykonuwany raz czy kilka razy w roznych odstepach czasu
                {
                    OneTimeDate = usp.OneTimeDate.Date;                                // przechowuje datę jednorazowego wykonania job'a
                    RecurigStartDate = dt.Date;
                }
                else
                {
                    OneTimeDate = dt.Date;                                // przechowuje datę jednorazowego wykonania job'a

                    RecurigStartDate = usp.RecuringStartDate1.Date;                  // przechowyje date rozpoczecia cyklicznego wykonywania job'a

                    occurs = usp.Occurs.ToString();                                           //co ile minut/godzin/dni/tygodni/miesiecy ma być wykonywane zapytanie
                    time = usp.Time.ToString();                                               // przechowuje dane na jaki czas oddziela poszczegolne zapytanie od zapytania
                }

                bool authentication = usp.Authentication;                          // przechowuje true lub false  czy autoryzacja ma byc windowsowa czy nie


                string jobName = usp.JobName.ToString();
                string description = usp.Description.ToString();

                sr.Close();

                File.Delete("C:\\ProgramData\\Schedulerv1\\" + wybranySerwer.ToString() + ".xml");
                StreamWriter wr = new StreamWriter("C:\\ProgramData\\Schedulerv1\\" + wybranySerwer.ToString() + ".xml", true);   //skierowanie do lokalizacji pliku (true oznacza mozliwosc dopisywania)
                UstawieniaSerweraPlik usp1 = new UstawieniaSerweraPlik();

                usp1.Username = username;                                     //przechowuje login do bazy danych
                usp1.Password = password;                                     //przechowyje haslo potrzebne do logowania do bazy danych
                usp1.NazwaSerwera = serverName;
                usp1.SQLserverName1 = SQLserverName;                                 //przechowuje nazwe instancji
                usp1.NazwaBazyDanych = nazwaBazyDanych;                       //przechowuje nazwe bazy danych
                usp1.ZapytanieSQL = zapytanieSQl;                                //przechowuje zapytanie sql
                usp1.Wlaczony = false;                                      // przechowuje true lub false czy job ma działac czy nie
                usp1.Schedule = schedule;                                      // przechowuje true lub false czy job ma byc wykonuwany raz czy kilka razy w roznych odstepach czasu
                usp1.OneTimeDate = OneTimeDate;                                // przechowuje datę jednorazowego wykonania job'a

                usp1.Authentication = authentication;                          // przechowuje true lub false  czy autoryzacja ma byc windowsowa czy nie
                usp1.RecuringStartDate1 = RecurigStartDate;                  // przechowyje date rozpoczecia cyklicznego wykonywania job'a

                usp1.Occurs = occurs;                                           //co ile minut/godzin/dni/tygodni/miesiecy ma być wykonywane zapytanie
                usp1.Time = time;                                               // przechowuje dane na jaki czas oddziela poszczegolne zapytanie od zapytania
                usp1.JobName = jobName;
                usp1.Description = description;



                serializer.Serialize(wr, usp1);
                wr.Flush();
                wr.Close();
            }
            catch (Exception ex)
            {
                WriteErrorLog.ErrorLog("błąd aktualizacji pliku : : : : : : : : : : : :" + ex.ToString());
            }

        }

        #endregion

        #region aktualizacja pliku xml BARDZO DO PRZEMYSLENIA NIE POWINNO TAK BYC, ALE NIE BYŁO CZASU INACZEJ
        public void aktualizujPlik(string czas, DateTime date, object wybranySerwer)        // ta funkcja ma za zadanie aktualizować plik xml       konkretnie chodzi o przycisk włącz - który ma być ustawiany na false
        {
            try
            {
                StreamReader sr = new StreamReader("C:\\ProgramData\\Schedulerv1\\" + wybranySerwer.ToString() + ".xml");

                XmlSerializer serializer = new XmlSerializer(typeof(UstawieniaSerweraPlik));
                UstawieniaSerweraPlik usp = (UstawieniaSerweraPlik)serializer.Deserialize(sr);

                string username = usp.Username.ToString();                                     //przechowuje login do bazy danych
                string password = usp.Password.ToString();                                     //przechowyje haslo potrzebne do logowania do bazy danych
                string serverName = wybranySerwer.ToString();
                string SQLserverName = usp.SQLserverName1.ToString();                                 //przechowuje nazwe instancji
                string nazwaBazyDanych = usp.NazwaBazyDanych.ToString();                       //przechowuje nazwe bazy danych
                string zapytanieSQl = usp.ZapytanieSQL.ToString();                                //przechowuje zapytanie sql
                bool wlaczony = usp.Wlaczony;                                      // przechowuje true lub false czy job ma działac czy nie
                bool schedule = usp.Schedule;

                DateTime OneTimeDate;

                DateTime RecurigStartDate;                  // przechowyje date rozpoczecia cyklicznego wykonywania job'a

                string occurs = "";                                          //co ile minut/godzin/dni/tygodni/miesiecy ma być wykonywane zapytanie
                string time = "";

                if (usp.Schedule == true)                                                            // przechowuje true lub false czy job ma byc wykonuwany raz czy kilka razy w roznych odstepach czasu
                {
                    OneTimeDate = date;                                // przechowuje datę jednorazowego wykonania job'a
                }
                else
                {
                    RecurigStartDate = date;                  // przechowyje date rozpoczecia cyklicznego wykonywania job'a
                    occurs = usp.Occurs.ToString();                                           //co ile minut/godzin/dni/tygodni/miesiecy ma być wykonywane zapytanie
                    time = usp.Time.ToString();                                               // przechowuje dane na jaki czas oddziela poszczegolne zapytanie od zapytania
                }

                bool authentication = usp.Authentication;                          // przechowuje true lub false  czy autoryzacja ma byc windowsowa czy nie


                string jobName = usp.JobName.ToString();
                string description = usp.Description.ToString();

                sr.Close();

                File.Delete("C:\\ProgramData\\Schedulerv1\\" + wybranySerwer.ToString() + ".xml");
                StreamWriter wr = new StreamWriter("C:\\ProgramData\\Schedulerv1\\" + wybranySerwer.ToString() + ".xml", true);   //skierowanie do lokalizacji pliku (true oznacza mozliwosc dopisywania)
                UstawieniaSerweraPlik usp1 = new UstawieniaSerweraPlik();

                usp1.Username = username;                                     //przechowuje login do bazy danych
                usp1.Password = password;                                     //przechowyje haslo potrzebne do logowania do bazy danych
                usp1.NazwaSerwera = serverName;
                usp1.SQLserverName1 = SQLserverName;                                 //przechowuje nazwe instancji
                usp1.NazwaBazyDanych = nazwaBazyDanych;                       //przechowuje nazwe bazy danych
                usp1.ZapytanieSQL = zapytanieSQl;                                //przechowuje zapytanie sql
                usp1.Wlaczony = wlaczony;                                      // przechowuje true lub false czy job ma działac czy nie
                usp1.Schedule = schedule;

                // przechowuje true lub false czy job ma byc wykonuwany raz czy kilka razy w roznych odstepach czasu

                if (usp.Schedule == true)                                                            // przechowuje true lub false czy job ma byc wykonuwany raz czy kilka razy w roznych odstepach czasu
                {
                    usp1.OneTimeDate = date;                                 // przechowuje datę jednorazowego wykonania job'a
                }
                else
                {
                    usp1.RecuringStartDate1 = date;                  // przechowyje date rozpoczecia cyklicznego wykonywania job'a
                    occurs = usp.Occurs.ToString();                                           //co ile minut/godzin/dni/tygodni/miesiecy ma być wykonywane zapytanie
                    time = usp.Time.ToString();                                               // przechowuje dane na jaki czas oddziela poszczegolne zapytanie od zapytania
                }
                // przechowuje datę jednorazowego wykonania job'a

                usp1.Authentication = authentication;                          // przechowuje true lub false  czy autoryzacja ma byc windowsowa czy nie
                                                                               // przechowyje date rozpoczecia cyklicznego wykonywania job'a

                usp1.Occurs = occurs;                                           //co ile minut/godzin/dni/tygodni/miesiecy ma być wykonywane zapytanie
                usp1.Time = time;                                               // przechowuje dane na jaki czas oddziela poszczegolne zapytanie od zapytania
                usp1.JobName = jobName;
                usp1.Description = description;



                serializer.Serialize(wr, usp1);
                wr.Flush();
                wr.Close();
                System.Threading.Thread.Sleep(50000);
            }
            catch (Exception ex)
            {
                WriteErrorLog.ErrorLog("Blad aktualizacji pliku  : : : : : : " + ex.ToString());
            }
        }

        #endregion


        public void sprawdzZmiane()
        {

        }
    }

}
