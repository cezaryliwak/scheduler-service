﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerService1
{
    class ConnectionDB
    {
        private SqlConnection polaczenie;   //obiekt widoczny tylko w całej klasie

        // Konstruktor d tworzenia połączenia za pomocą autoryzacji SQL Server
        //uzytkownik, haslo, nazwa istancji, nazwa bazy danych

        public ConnectionDB(string user, string password, string instance, string dbdir)
        {
            try
            {
                polaczenie = new SqlConnection();
                polaczenie.ConnectionString = "user id=" + user + ";" + //uzytkownik login
                    "password=" + password + ";" +      //haslo
                    "Data Source=" + instance + ";" +    //nazwa sqrewra bazy danych
                    "Trusted_Connection=no" + ";" +
                    "database=" + dbdir + ";" +         //nazwa bazy danych
                    "connection timeout=3";

            }
            catch (Exception e)
            {
                Console.WriteLine("Połączenie do serwera błąd " + e);
            }
        }

        //Konstruktor do tworzenia połączenia za pomocą autoryzacji widows
        // nazwa istancji, nazwa bazy danych
        public ConnectionDB(string instance, string dbdir)
        {
            polaczenie = new SqlConnection();
            polaczenie.ConnectionString = "Data Source=" + instance + ";" +
                "Trusted_Connection=yes" + "; " +
                "database=" + dbdir + "; " +
                "connection timeout=3";
        }

        public void zapytanie(string query, string wybranySerwer)
        {
            try
            {
                polaczenie.Open();
                DataTable dt = new DataTable(); //deklaracja i utworzenie instancji obiektu DataTable 
                SqlDataReader sdr;              //deklaracja obiektu SqlDataReader
                SqlCommand sqlc;                //Deklaracja obiektu SqlCommand
                sqlc = new SqlCommand(query);       //utworzenie instancji SQLCommand ktora ma wykonac zapytanie podane jako parametr 

                sqlc.Connection = this.polaczenie;  //wskazanie polaczenia do bazy danych
                sdr = sqlc.ExecuteReader();          //wykonianie zapytania i utworzenie wskaźnika dr
                WriteErrorLog.ErrorLog("Wykonano zapytanie SQL na serwerze  :   " + wybranySerwer.ToString());
                //   ZapytaniaClass zc = new ZapytaniaClass();
                //      zc.aktualizujPlik(wybranySerwer.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Błąd o tresci " + e);
            }
        }

    }
}
