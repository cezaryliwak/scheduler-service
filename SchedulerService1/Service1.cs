﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using System.Timers;
using static System.Net.Mime.MediaTypeNames;

namespace SchedulerService1


{
    public partial class Service1 : ServiceBase
    {

        int iloscLinii = 0;
        private System.Timers.Timer timer1 = null;
        List<String> jobs = new List<string>();
        Thread thr;
        int licznik = 0;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            if (File.Exists("C:\\ProgramData\\Schedulerv1\\nazwyJob.txt"))
            {
                TextReader czytaj = new StreamReader("C:\\ProgramData\\Schedulerv1\\nazwyJob.txt");

                int iloscLinii = File.ReadAllLines("C:\\ProgramData\\Schedulerv1\\nazwyJob.txt").Length;
                for (int i = 0; i < iloscLinii; i++)
                {
                    string tekst = czytaj.ReadLine();
                    uruchom(tekst);
                    WriteErrorLog.ErrorLog("Serwis został uruchomiony");
                    WriteErrorLog.ErrorLog("Uruchomiony Job: " + tekst);
                }
               
            }
            //timer1 = new System.Timers.Timer();
            //this.timer1.Interval = 1000;
            //this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            //timer1.Enabled = true;

        }

        protected override void OnStop()
        {
            Dispose();
            WriteErrorLog.ErrorLog("Serwis został zatrzymany");
        }
        private void uruchom(string job)
        {
            TrescWatku tw = new TrescWatku();
            Thread thr = new Thread(tw.zapytanie);
            //string a = job+ "\t " + thr.ManagedThreadId;
            thr.Start(job);
        }
        public void sprawdzUruchomione()
        {
            do
            {
                TextReader tr = new StreamReader("C:\\ProgramData\\Schedulerv1\\nazwyJob.txt");
                System.Threading.Thread.Sleep(10000);
            } while (1 == 1);
        }
    }
}
