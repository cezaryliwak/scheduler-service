﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerService1
{
    public class UstawieniaSerweraPlik
    {
        string nazwaSerwera;
        string SQLserverName;
        bool authentication;
        string username;
        string password;
        string jobName;
        string description;
        bool schedule;
        DateTime oneTimeDate;
        DateTime RecuringStartDate;
        string zapytanieSQL;
        string nazwaBazyDanych;

        string occurs;
        string time;
        bool wlaczony;

        public string SQLserverName1
        {
            get
            {
                return SQLserverName;
            }

            set
            {
                SQLserverName = value;
            }
        }

        public bool Authentication
        {
            get
            {
                return authentication;
            }

            set
            {
                authentication = value;
            }
        }

        public string Username
        {
            get
            {
                return username;
            }

            set
            {
                username = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public string JobName
        {
            get
            {
                return jobName;
            }

            set
            {
                jobName = value;
            }
        }

        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public bool Schedule
        {
            get
            {
                return schedule;
            }

            set
            {
                schedule = value;
            }
        }

        public DateTime OneTimeDate
        {
            get
            {
                return oneTimeDate;
            }

            set
            {
                oneTimeDate = value;
            }
        }



        public string Occurs
        {
            get
            {
                return occurs;
            }

            set
            {
                occurs = value;
            }
        }

        public string Time
        {
            get
            {
                return time;
            }

            set
            {
                time = value;
            }
        }

        public bool Wlaczony
        {
            get
            {
                return wlaczony;
            }

            set
            {
                wlaczony = value;
            }
        }

        public DateTime RecuringStartDate1
        {
            get
            {
                return RecuringStartDate;
            }

            set
            {
                this.RecuringStartDate = value;
            }
        }

        public string ZapytanieSQL
        {
            get
            {
                return zapytanieSQL;
            }

            set
            {
                zapytanieSQL = value;
            }
        }

        public string NazwaBazyDanych
        {
            get
            {
                return nazwaBazyDanych;
            }

            set
            {
                nazwaBazyDanych = value;
            }
        }

        public string NazwaSerwera
        {
            get
            {
                return nazwaSerwera;
            }

            set
            {
                nazwaSerwera = value;
            }
        }
    }
}
